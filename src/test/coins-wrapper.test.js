import {afterAll, beforeAll, expect} from '@jest/globals';
import * as data from '../assets/coins-data.json';
import {coinsWrapper} from '../coins-wrapper';

let expectedOutput;

beforeAll(() => expectedOutput = (`1. Jednogroszówki: - rulony: 40 - pozostałe: 34
2. Dwugroszówki: - rulony: 40 - pozostałe: 10
3. Pięciogroszówki: - rulony: 55 - pozostałe: 13
4. Dziesięciogroszówki: - rulony: 33 - pozostałe: 40
5. Dwudziestogroszówki: - rulony: 85 - pozostałe: 11
6. Pięćdziesieciogroszówki: - rulony: 42 - pozostałe: 12`));

test('coins-wrapper-result', () => expect(coinsWrapper(data.coins)).toBe(expectedOutput));

afterAll(() => expectedOutput = undefined);
