export const coinsWrapper = (array) => {
    const rollsProperties = {
        1: {rollSize: 40, name: 'Jednogroszówki'},
        2: {rollSize: 40, name: 'Dwugroszówki'},
        5: {rollSize: 30, name: 'Pięciogroszówki'},
        10: {rollSize: 50, name: 'Dziesięciogroszówki'},
        20: {rollSize: 20, name: 'Dwudziestogroszówki'},
        50: {rollSize: 40, name: 'Pięćdziesieciogroszówki'}
    };
    const result = array
        .reduce((acum, penny) => {
            acum[penny] = {
                quantity: acum[penny]?.quantity + 1 || 1,
                rolls: Math.floor((acum[penny]?.quantity + 1) / rollsProperties[penny].rollSize),
                rest: (acum[penny]?.quantity + 1) % rollsProperties[penny].rollSize
            }
            return acum;
        }, {})
    return Object.keys(result)
        .map((row, index) => `${index + 1}. ${rollsProperties[row].name}: - rulony: ${result[row].rolls} - pozostałe: ${result[row].rest}`).join('\n');
}
